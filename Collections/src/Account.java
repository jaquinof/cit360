import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Account {

    String accountType;
    String givenName;
    String familyName;

    public Account(String accType, String name, String family){
        this.accountType =accType;
        this.givenName = name;
        this.familyName = family;
    }

    public static void main (String args[]) {

        String csvFile = "src/rawData.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        SortedMap<String, Account> sortMap = new TreeMap<>();




        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] data = line.split(cvsSplitBy);
                Account a = new Account(data[1], data[2], data[3]);
                sortMap.put(data[0], a);

            }
            Scanner reader = new Scanner(System.in);  // Reading from System.in
            System.out.println("Enter an account number: ");
            String n = reader.nextLine();
            //once finished
            reader.close();

            if (sortMap.containsKey(n)) {
                System.out.println("Account exists");

            } else {
                System.out.println("Account does not exist");
                System.out.println("...");
                System.out.println("...");
                System.out.println("...");
                System.out.println("Creating a new account");
                System.out.println("...");
                System.out.println("...");
                Account newA = new Account("Business","Jordan","Aquino");
                sortMap.put(n, newA);
            }

            Thread.sleep(2000);
            for (Map.Entry<String , Account> entry : sortMap.entrySet()) {
                System.out.println("Account Number: " + entry.getKey() + ", Account Type: " + entry.getValue().accountType + ", Given Name: " + entry.getValue().givenName + ", Family Name: " + entry.getValue().familyName);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e){
            e.printStackTrace();
        }finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }



    }
}
