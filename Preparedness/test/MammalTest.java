import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MammalTest {
    @Test
    public void testLegCount (){
       MammalBean testL = new MammalBean(3,"pink", 6);
       assertEquals(3,testL.getLegCount());
        MammalBean testL2 = new MammalBean(4,"pink", 6);
        assertEquals(4,testL2.getLegCount());
    }

    @Test
    public void testColor(){
        MammalBean testC = new MammalBean(5, "purple", 8);
        assertEquals("purple", testC.getColor());
    }

    @Test
    public void testHeight(){
        double delta = 0.1;
        MammalBean testH = new MammalBean(5, "purple", 8);
        assertEquals(8, testH.getHeight(),delta);
        MammalBean testH2 = new MammalBean(5, "purple", 20);
        assertEquals(20, testH2.getHeight(),delta);
    }
}
