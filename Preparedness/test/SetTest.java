import org.junit.Test;

import java.util.*;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertFalse;

public class SetTest {
    @Test
    public void setMammal() {
        MammalBean[] MammalBeans = new MammalBean[5];
        MammalBeans[0] = new MammalBean(4, "blue", 6);
        MammalBeans[1] = new MammalBean(5, "orange", 3);
        MammalBeans[2] = new MammalBean(2, "purple", 7);
        MammalBeans[3] = new MammalBean(3, "green", 6);
        MammalBeans[4] = new MammalBean(6, "white", 9);

        Set<MammalBean> mySet = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            mySet.add(MammalBeans[i]);
        }
        for (MammalBean testMammal : mySet) {
            assertTrue(mySet.contains(testMammal));
        }

        mySet.remove(MammalBeans[4]);//removing the last element
        mySet.remove(MammalBeans[3]);//removing the last element
        assertFalse(mySet.contains(MammalBeans[4]));
        assertFalse(mySet.contains(MammalBeans[3]));
    }

    @Test
    public void mapDog(){
        DogBean[] Dogs = {new DogBean(2,"blue",5.3,"akita","guau"),
                new DogBean(2,"orange",6.3,"terrie","wally"),
                new DogBean(4,"black",7.2,"shepherd","abc"),
                new DogBean(100,"purple",1.3,"azawakh","boby")};

        Map<String,DogBean> myHashMap = new HashMap<>();

        for(int i=0; i<Dogs.length; i++){
            myHashMap.put(Dogs[i].getName(),Dogs[i]);
        }

        for (DogBean testDog : Dogs){
            assertTrue(myHashMap.containsValue(testDog));
        }

        myHashMap.remove("wally");
        myHashMap.remove("boby");

        assertFalse(myHashMap.containsKey("wally"));
        assertFalse(myHashMap.containsKey("boby"));

    }
}
