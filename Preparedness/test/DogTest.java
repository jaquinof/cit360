import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class DogTest {
    @Test
    public void TestLeg(){
        DogBean testL = new DogBean(5,"blue",8,"abc","dj");
        assertEquals(5, testL.getLegCount());

        DogBean testL2 = new DogBean(7,"blue",8,"abc","dj");
        assertEquals(7, testL2.getLegCount());
    }

    @Test
    public void TestColor(){
        DogBean testC = new DogBean(6,"orange",5,"def","xd");
        assertEquals("orange",testC.getColor());

        DogBean testC2 = new DogBean(6,"white",5,"def","xd");
        assertEquals("white",testC2.getColor());
    }

    @Test
    public void TestHeight(){
        DogBean testH = new DogBean(7,"pink",5,"oer","Hi");
        assertEquals(5, testH.getHeight(), 0.1);

        DogBean testH2 = new DogBean(7,"pink",9,"oer2","Hii");
        assertEquals(9, testH2.getHeight(), 0.1);
    }

    @Test
    public void TestBreed(){
        DogBean testB = new DogBean(7,"pink",5,"oer","Hi");
        assertEquals("oer", testB.getBreed());

        DogBean testB2 = new DogBean(7,"pink",9,"oer2","Hii");
        assertEquals("oer2", testB2.getBreed());
    }

    @Test
    public void TestName(){
        DogBean testN = new DogBean(7,"pink",5,"oer","Hi");
        assertEquals("Hi", testN.getName());

        DogBean testN2 = new DogBean(7,"pink",9,"oer2","Hii");
        assertEquals("Hii", testN2.getName());
    }
}
