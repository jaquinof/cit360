public class MammalBean {
    private int legCount;
    private String color;
    private double height;

    MammalBean(int l, String c, double h){
        legCount = l;
        color = c;
        height = h;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
