public class DogBean extends MammalBean{
    private String breed;
    private String name;

    public DogBean(int l, String c, double h, String b, String n){
        super(l, c, h);
        breed = b;
        name = n;
    }

    public String toString() {
        return "DogBean: LegCount:" + super.getLegCount() + ", Color:" + super.getColor() + ", height:" + super.getHeight() + ", breed:" + getBreed() + ", name:" + getName() + "\n";
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
