public class Kennel {
    public DogBean[] buildDogs (){
        DogBean[] DogBeans = new DogBean[5];
        DogBeans[0] = new DogBean(2,"blue",5.3,"akita","guau");
        DogBeans[1] = new DogBean(2,"orange",6.3,"terrie","wally");
        DogBeans[2] = new DogBean(4,"black",7.2,"shepherd","abc");
        DogBeans[3] = new DogBean(3,"white",8.2,"australian cattle","abc32");
        DogBeans[4] = new DogBean(100,"purple",1.3,"azawakh","boby");

        return DogBeans;
    }

    public void displayDogs(DogBean[] dogBeans){
        for(DogBean dogBean : dogBeans){
            System.out.printf(dogBean.toString());
        }
    }

    public static void main (String[] args){
        Kennel obj = new Kennel();
        DogBean[] allDogs = obj.buildDogs();
        obj.displayDogs(allDogs);

    }

}
